package fr.umfds.TPtestServicesREST;

import java.util.ArrayList;


import javax.xml.bind.annotation.XmlRootElement;



@XmlRootElement(name = "brainstorm")
public class Brainstorm implements java.lang.Comparable<Brainstorm>{
	
private String nom;
private int identifiant;
private ArrayList<Idea> ideas;


public Brainstorm() {}

public Brainstorm(String nom,int identifiant,ArrayList<Idea> ideas) {
	this.setNom(nom);
	this.setIdentifiant(identifiant);
	this.setIdeas(ideas);
}

public int getIdentifiant() {
	return identifiant;
}
public int compareTo(Brainstorm b) {
	return this.getNom().compareTo(b.getNom());
}
	
public void setIdentifiant(int identifiant) {
	this.identifiant = identifiant;
}

public ArrayList<Idea> getIdeas() {
	return ideas;
}

public void setIdeas(ArrayList<Idea> ideas) {
	this.ideas = ideas;
}

public String getNom() {
	return nom;
}

public void setNom(String nom) {
	this.nom = nom;
}





}
