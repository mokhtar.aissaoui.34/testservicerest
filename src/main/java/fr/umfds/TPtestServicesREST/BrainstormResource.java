package fr.umfds.TPtestServicesREST;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/brainstorms")
@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON }) 
public class BrainstormResource  {


@Inject
private BrainstormDB db= new BrainstormDB();;

public BrainstormResource() {
	
}
public BrainstormResource(BrainstormDB db){
	this.db=db;
}
@GET
public ArrayList<Brainstorm> getBrainstorms(){
	ArrayList<Brainstorm> currentBrainstorms = new ArrayList<Brainstorm>();
	currentBrainstorms = db.getBrainstorms();
	Collections.sort(currentBrainstorms);
	
	return currentBrainstorms;
}
@GET
@Path("brainstormid-{id}")
public Brainstorm getBrainstorm(@PathParam("id")int brainstormId) {
	for(int i =0;i<db.getBrainstorms().size();i++) {
		if(db.getBrainstorms().get(i).getIdentifiant()== brainstormId) {
			return db.getBrainstorms().get(i);
		}
	}
	return null;
	
}

public BrainstormDB getDb() {
	return db;
}

public void setDb(BrainstormDB db) {
	this.db = db;
}


}
