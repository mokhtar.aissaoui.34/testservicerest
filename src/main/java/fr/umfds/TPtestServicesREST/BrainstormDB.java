package fr.umfds.TPtestServicesREST;

import java.util.ArrayList;




public class BrainstormDB {
	public  ArrayList<Brainstorm> brainstorms= new ArrayList<Brainstorm>();
	
	public BrainstormDB() {
		brainstorms= new ArrayList<Brainstorm>(){{ add (new Brainstorm("test",1,new ArrayList<Idea>()));add(new Brainstorm("abx",2,new ArrayList<Idea>()));add(new Brainstorm("bad",3,new ArrayList<Idea>()));}};
	}
	

	public void addBrainstormToBD(Brainstorm b){
		brainstorms.add(b);
		
	}	
	

	public  ArrayList<Brainstorm> getBrainstorms() {
	
		
		return brainstorms;
	}
}
