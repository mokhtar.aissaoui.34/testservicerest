package fr.umfds.TPtestServicesREST;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;



public class ITintegrationTest extends JerseyTest {
 
    
 
	protected BrainstormResource Br = Mockito.mock(BrainstormResource.class);
   
protected Application configure() {
    ResourceConfig resourceConfig = new ResourceConfig(BrainstormResource.class);
    resourceConfig.property(LoggingFeature.LOGGING_FEATURE_LOGGER_LEVEL_SERVER, Level.WARNING.getName());
            
    resourceConfig.register(new AbstractBinder() {
        @Override
        protected void configure() {

            ArrayList<Brainstorm> l=new ArrayList<Brainstorm>(){{ add (new Brainstorm("test",1,new ArrayList<Idea>()));add(new Brainstorm("abx",2,new ArrayList<Idea>()));add(new Brainstorm("bad",3,new ArrayList<Idea>()));}};
            Mockito.when(Br.getBrainstorms()).thenReturn(l);
            Mockito.when(Br.getBrainstorm(1)).thenReturn(new Brainstorm("test",1,new ArrayList<Idea>()));
            Mockito.when(Br.getBrainstorm(4)).thenReturn(null);
            bind(Br).to(BrainstormDB.class);

            }
            });
            return resourceConfig;
        }
     @Test
    public void testGetBrainstorms() {
        // Given

        // When
        Response response = target("/brainstorms").request(MediaType.APPLICATION_JSON_TYPE).get();

        // Then
        Assert.assertEquals("Http Response should be 200: ", Status.OK.getStatusCode(), response.getStatus());
        ArrayList<Brainstorm> readEntities = response.readEntity(new GenericType<ArrayList<Brainstorm>>() {});
        Assert.assertNotNull(readEntities);
        Assert.assertEquals(3, readEntities.size());
        Assert.assertTrue(readEntities.stream().anyMatch(current -> current.getIdentifiant()==1));
    }
     @Test
     public void testGetBrainstorm() {
         // Given

         // When
         Response response = target("/brainstorms/brainstormid-1").request(MediaType.APPLICATION_JSON_TYPE).get();

         // Then
         Assert.assertEquals("Http Response should be 200: ", Status.OK.getStatusCode(), response.getStatus());
         Brainstorm readEntities = response.readEntity(new GenericType<Brainstorm>() {});
         Assert.assertNotNull(readEntities);
         Assert.assertEquals(readEntities.getNom(), Br.getBrainstorm(1).getNom());
         
     }
     @Test
     public void testGetBrainstormNull() {
         // Given

         // When
         Response response = target("/brainstorms/brainstormid-6").request(MediaType.APPLICATION_JSON_TYPE).get();

         // Then
         Assert.assertEquals("Http Response should be 204: ", 204, response.getStatus());
         Brainstorm readEntities = response.readEntity(new GenericType<Brainstorm>() {});
         Assert.assertNull(readEntities);

         
     }

}
