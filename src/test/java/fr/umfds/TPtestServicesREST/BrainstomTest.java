package fr.umfds.TPtestServicesREST;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.mockito.*;

public class BrainstomTest{

	@Test
	public void TestBd() {
		BrainstormResource br = new BrainstormResource(new BrainstormDB());
		ArrayList<Brainstorm> b = br.getBrainstorms();
		assertTrue(b!=null);
	}
	@Test
	public void TestBdMock() {
		//Given
		BrainstormResource dbMock = Mockito.mock(BrainstormResource.class);
		//when
		ArrayList<Brainstorm> b = dbMock.getBrainstorms();
		//then
		assertNotNull(b);
		for(int i=0;i<b.size()-1;i++) {
			//then
			assertTrue(b.get(i).getNom().compareTo(b.get(i+1).getNom())<0);
		}
	}

}
